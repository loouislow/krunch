#!/bin/bash
#
# Krunch
#
# Web Builder with Grunt :
# js/css (minifier, compressor, uglifier),
# js (merger, compiler)
#
# @@script: install.sh
# @@description: krunch installer
# @@version: 0.0.0.2
# @@author: Loouis Low
# @@copyright: Goody Technologies
#

function runas_root() {
	if [ "$(whoami &2>/dev/null)" != "root" ] && [ "$(id -un &2>/dev/null)" != "root" ]
		then
			echo -e "[krunch] permission denied."
			exit 1
	fi
}

function reset() {
	echo "[krunch] reseting data..."
	find . -name "node_modules" -type d -exec rm -r "{}" \;
}

function check_node() {
	if which nodejs > /dev/null;
	then
		echo "[krunch] node installed!"
	else
		echo "[krunch] installing node..."
		curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -
		apt-get install -y nodejs
		npm install -g n grunt

		echo "[krunch] getting latest n..."
		n latest
	fi
}

function check_buildtools() {
	if which build-essential > /dev/null;
	then
		echo "[krunch] build tools installed!"
	else
		echo "[krunch] installing build tools..."
		apt-get install -y build-essential
	fi
}

function check_modules() {
	echo "[krunch] getting modules..."
	npm install
}

function end() {
	echo "[krunch] --- all done ---"
}

### init
runas_root
reset
check_node
check_buildtools
check_modules
end
