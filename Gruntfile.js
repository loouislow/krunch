//
// Krunch
//
// Web Builder with Grunt :
// js/css (minifier, compressor, uglifier),
// js (merger, compiler)
//
// @@script: Gruntfile.js
// @@description: task configuration
// @@version: 0.0.0.2
// @@author: Loouis Low
// @@copyright: Goody Technologies
//

var colors = require('colors');

/***
 ** Configurations
 */

module.exports = function(grunt) {
grunt.initConfig({
	pkg: grunt.file.readJSON('package.json'),
    // -------------------------------------------
	// script decompressor
    // -------------------------------------------
  uglify: {
	    build: {
				expand: true,
			// will look for all js files in specific folder
		    cwd: 'src/js/',
		    src: ['**/*.js'],
		    dest: 'src/js/',
		    ext: '.min.js'
	    }
	},
    // -------------------------------------------
	// image optimizer
    // -------------------------------------------
	imagemin: {
		dynamic: {
		    files: [{
		      expand: true,
		      // will look for all image files in specific folder
		      cwd: 'src/img/',
		      src: ['**/*.{png,jpg,gif}'],
		      dest: 'src/img/'
		    }]
		  }
	},
	// -------------------------------------------
	// css minifier
    // -------------------------------------------
	cssmin: {
	  minify: {
	    expand: true,
	    // will look for all css files in specific folder
	    cwd: 'src/css/',
	    src: ['**/*.css'],
	    dest: 'src/css/',
	    ext: '.min.css'
	  }
	},
	  // -------------------------------------------
	  // js custom merge compiler
	  // for "application.min.js"
 	  // -------------------------------------------
	concat: {
		options: {
		      separator: ';'
		    },
	  js: {
	  // Compile js files with priority
	    src: [
				'src/js/01.js',
				'src/js/02.js',
				'src/js/03.js',
	    ],
	    dest: 'src/js/application.min.js'
	  }
	}
});

/***
 ** Initializer
 */

console.log('');

console.log(colors.cyan('[krunch] task >> decompressing >> script'));
grunt.loadNpmTasks('grunt-contrib-uglify');

console.log(colors.cyan('[krunch] task >> optimizing >> image'));
grunt.loadNpmTasks('grunt-contrib-imagemin');

console.log(colors.cyan('[krunch] task >> compressing >> script'));
grunt.loadNpmTasks('grunt-contrib-cssmin');

console.log(colors.cyan('[krunch] task >> merging >> script '));
grunt.loadNpmTasks('grunt-contrib-concat');

console.log(colors.cyan('[krunch] tasks preview...'));
grunt.registerTask('default', ['uglify', 'imagemin', 'cssmin', 'concat']);

console.log('');

};
