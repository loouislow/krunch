# Krunch

> Web Builder with Grunt : js/css (minifier, compressor, uglifier), js (merger, compiler)

## Feature

- actual build project is separated by compiled version
- uglify js (js files that min-ed needs to be un-mined before file merging)
- minify css,js (compress css,js files)
- merge js (combine multiple js files in order)
- build project package with a shortcut linked to desktop

## Workflow
```text

                 > [html(bypass)] ------------------------>
                 > [css] ------------> [minify] ---------->
[source project] > [js]  ------------> [ugligy/minify] ---> [compiled folder]
                 > [img] ------------> [optimize] -------->
                 > [font(bypass)] ------------------------>
```

## Prerequisite

To run the automatic setup for the first time, type in the `Terminal`

```bash
$ bash setup.sh
```
Or,

```bash
$ ./setup.sh
```
## Configurations

### `Grunfiles.js`

**To decompress js files**

```javascript
// script decompressor
uglify: {
	build: {
		expand: true,
		cwd: 'src/js/',
		src: ['**/*.js'],
		dest: 'src/js/',
		ext: '.min.js'
	}
}
```

**To optimize image files**

```javascript
// image optimizer
imagemin: {
	dynamic: {
		files: [{
			expand: true,
			cwd: 'src/img/',
			src: ['**/*.{png,jpg,gif}'],
			dest: 'src/img/'
		}]
	}
},
```

**To minify CSS files**

```javascript
// css minifier
cssmin: {
	minify: {
		expand: true,
		cwd: 'src/css/',
		src: ['**/*.css'],
		dest: 'src/css/',
		ext: '.min.css'
	}
},
```

**To compile merged files**

```javascript
// js custom merge compiler
// for "application.min.js"
concat: {
	options: {
		separator: ';'
	},
	js: {
		src: [
			'src/js/01.js',
			'src/js/02.js',
			'src/js/03.js',
		],
		dest: 'src/js/application.min.js'
	}
}
```

**To change output project name**

### `build.sh`

```bash
# change your project name
projectname="projectname"
```

## Usage

Create or move your new project to folder `src`. To build the project, type in the Terminal

```bash
$ bash build.sh
```
Or,

```bash
$ ./build.sh
```
