#!/bin/bash
#
# Krunch
#
# Web Builder with Grunt :
# js/css (minifier, compressor, uglifier),
# js (merger, compiler)
#
# @@script: build.sh
# @@description: krunch builder
# @@version: 0.0.0.1
# @@author: Loouis Low
# @@copyright: Goody Technologies
#

# change your project name
projectname="projectname"

function build() {
	echo -e "\e[96m[krunch] building project..."
	grunt
}

function show_header() {
	echo -e "\e[96m-----------------------------------------------------"
	echo -e "\e[96mKrunch - tiny web builder - v0.0.37"
	echo -e "\e[96m-----------------------------------------------------"
	echo ""
}

function make_dist() {
	if [ -d dist ] ; then
		mkdir -p dist/$projectname/assets
		cp -r src/css dist/$projectname/assets
		cp -r src/js dist/$projectname/assets
		cp -r src/img dist/$projectname/assets
		cp -r src/font dist/$projectname/assets
		cp -r src/*.html dist/$projectname

		echo ""
		echo -e "\e[96m[krunch] --- asset moved to /home/`whoami`/Desktop ---"
		ln -s $PWD/dist /home/`whoami`/Desktop
		echo ""
	else
		mkdir -p dist/$projectname/assets
		cp -r src/css dist/$projectname/assets
		cp -r src/js dist/$projectname/assets
		cp -r src/img dist/$projectname/assets
		cp -r src/font dist/$projectname/assets
		cp -r src/*.html dist/$projectname

		echo ""
		echo -e "\e[96m[krunch] --- asset moved to /home/`whoami`/Desktop ---"
		ln -s $PWD/dist/$projectname /home/`whoami`/Desktop
		echo -e "\e[96m[krunch] just ignore the ln error, notihng to be worried."
	fi
}

### init
show_header
build
make_dist
